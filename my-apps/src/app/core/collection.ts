import { State} from '../shared/enums/state.enum';
import { Item } from '../shared/models/item.model';

export const COLLECTION: Item[] = [
  {
    id : 'a1',
    name : 'sylvie',
    reference : '123',
    state : State.ENCOURS
  },
  {
    id : 'a2',
    name : 'nadine',
    reference : '489',
    state : State.ALIVRER
  },  {
    id : 'a3',
    name : 'marc',
    reference : '146',
    state : State.LIVRE
  }
];
