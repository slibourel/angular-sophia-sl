import { Injectable } from '@angular/core';
import { Item } from '../../../shared/models/item.model';
import { COLLECTION } from '../../collection';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CollectionService {
itemsCollection: AngularFirestoreCollection<Item>;
private _collection: Observable<Item[]>;
  constructor(private afs: AngularFirestore,
              private http: HttpClient) {
    this.itemsCollection = afs.collection<Item>('collection');
    this._collection = this.itemsCollection.valueChanges();
    // this._collection = this.http.get('url_api/collection');
   }

  /**
   *  get collection
   */
  get collection(): Observable<Item[]> {
    return this._collection;
    // return this.http.get('url_api/collection');
  }

  /**
   * set collection
   */
  set collection(tab: Observable<Item[]>) {
    this._collection = tab;
  }

  /**
   * get elem of collection by id
   */
  getCollectionById(id: string): string {
    return this._collection[id];
  }

  // add Item
  add(item: Item): void {
    item.id = this.afs.createId();
    this.itemsCollection.doc(item.id).set(item)
      .catch(error => console.log(error));
  }

  // update Item
  update(item: Item): void {
    this.itemsCollection.doc(item.id).update(item)
      .catch(error => console.log(error));
  }

  // delete Item
  delete(item: Item): void {
    this.itemsCollection.doc(item.id).delete()
      .catch(error => console.log(error));
  }

  // get data on item
  getItem(id: string): Observable<Item> {
    const item = this.afs.doc<Item>(`collection/${id}`).valueChanges();
    return item;
  }
}
