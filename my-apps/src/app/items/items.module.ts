import { AddItemComponent } from './containers/add-item/add-item.component';
import { CommonModule } from '@angular/common';
import { ItemsRoutingModule } from './items-routing.module';
import { ListItemsComponent } from './containers/list-items/list-items.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule, SharedModule, ItemsRoutingModule
  ],
  declarations: [ListItemsComponent, AddItemComponent],
  exports: [ListItemsComponent, AddItemComponent]
})
export class ItemsModule { }
