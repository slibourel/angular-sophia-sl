export enum State {
  ALIVRER = 'à livrer',
  ENCOURS = 'en cours',
  LIVRE = 'livré'
}
