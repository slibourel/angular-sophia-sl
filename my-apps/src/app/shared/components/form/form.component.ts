import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { State } from '../../enums/state.enum';
import { Item } from '../../models/item.model';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  libelles = Object.values(State);
  form: FormGroup;
  @Output() newItem: EventEmitter<Item> = new EventEmitter<Item>();
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
   this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
     name: ['', Validators.compose([Validators.required, Validators.minLength(5) ])],
     reference: ['', Validators.compose([Validators.required, Validators.minLength(5) ])],
     state: State.ALIVRER
    });
  }
  process (): void {
    console.log(this.form.value);
    this.newItem.emit(this.form.value);
    this.form.reset();
    this.form.get('state').setValue(State.ALIVRER);
  }

  isError(champs: string): boolean {
    return this.form.get(champs).invalid && this.form.get(champs).touched;
  }

}
