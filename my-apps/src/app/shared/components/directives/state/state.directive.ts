import { Directive, Input, OnInit, OnChanges, HostBinding} from '@angular/core';
import { State } from '../../../enums/state.enum';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit, OnChanges {
  @Input() appState: State;
  @HostBinding('class') nomClass: string;
  constructor() { console.log(this.appState); }

  ngOnInit() {
    console.log(this.appState);
    this.nomClass = this.formatCssClass(this.appState);
  }

  ngOnChanges() {
    console.log(this.appState);
    this.nomClass = this.formatCssClass(this.appState);
  }

  removeAccent(etat: string): string {
    return etat.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  formatCssClass(chaine: string): string {
    return `state-${this.removeAccent(chaine).toLowerCase().replace(' ', '')}`;
  }
}
