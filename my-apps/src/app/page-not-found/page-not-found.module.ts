import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoutingPageNotFoundModule } from './routing-page-not-found.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingPageNotFoundModule
  ],
  declarations: [PageNotFoundComponent],
  exports: [PageNotFoundComponent]
})
export class PageNotFoundModule { }
